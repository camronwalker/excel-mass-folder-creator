Attribute VB_Name = "CreateFolders"
Option Explicit

Sub Create_Folders()
    Dim Main_Parent As String
    Dim Folder_Creation_Array As Variant
    Dim Total_Folders As Long, Rw As Long
        
    Main_Parent = GetFolder
    'Debug.Print Main_Parent
start:
    Folder_Creation_Array = ActiveSheet.ListObjects("Folder_Creation_Table").DataBodyRange.Value
    Total_Folders = Application.WorksheetFunction.Max(ActiveSheet.ListObjects("Folder_Creation_Table").ListColumns("Folder ID Num").Range.Value)
    
    On Error Resume Next
    For Rw = 1 To Total_Folders
        If Folder_Creation_Array(Rw, 3) = 0 Then
            MkDir Main_Parent & "\" & Folder_Creation_Array(Rw, 2)
        Else
            MkDir Main_Parent & "\" & Folder_Creation_Array(Folder_Creation_Array(Rw, 3), 2) & "\" & Folder_Creation_Array(Rw, 2)
        End If
    Next Rw
        
    Debug.Print Err.Description
    If Err = 76 Then
        GoTo start
    End If
End Sub

Function GetFolder() As String
    Dim fldr As FileDialog
    Dim sItem As String
    Set fldr = Application.FileDialog(msoFileDialogFolderPicker)
    With fldr
        .Title = "Select a Folder"
        .AllowMultiSelect = False
        .InitialFileName = Application.DefaultFilePath
        If .Show <> -1 Then GoTo NextCode
        sItem = .SelectedItems(1)
    End With
NextCode:
    GetFolder = sItem
    Set fldr = Nothing
End Function
